# filestore-mediatheque

Groupe : Anne-Laure CHARLES - Louis MASSICARD - Claire ROMANEL - Adel YACIA

# Miage.20 FileStore

Sources for project miage.20

### Start and configure keycloak

In order to authenticate users we will use an already existing IdP : keycloak (https://www.keycloak.org/)

Use docker to run an instance and map a local folder to this container, allowing to persist data between restarts

```bash
docker pull jboss/keycloak:9.0.0
docker run --name keycloak -p 8080:8080 -v /var/keycloak:/opt/jboss/keycloak/standalone/data jboss/keycloak:9.0.0
docker exec keycloak keycloak/bin/add-user-keycloak.sh -u admin -p admin123
docker restart keycloak
```

Try then to access http://localhost:8080/auth and configure realm :

- Create a new realm 'Filestore' 
- Add a client named filestore (Direct Access Grant has to be enabled for the client)


### Start the application : 

Compile the projet, initilise db with default users (admin/admin, user/user) and start the application.

```bash
mvn clean package -DskipTests
mvn liquibase:migrate
java -cp ~/.m2/repository/com/h2database/h2/1.4.196/h2-1.4.196.jar org.h2.tools.RunScript -url jdbc:h2:~/.filestore/filestore -user sa -password sa -script ./src/main/resources/scripts/users.sql -showResults
java -jar ./target/jayblanc-1.0.0-SNAPSHOT-thorntail.jar -Dswarm.port.offset=100
```

Open browser to : http://localhost:8180/fs/api/files

### Start another instance of the application (neighbourhood testing) : 

```bash
mvn liquibase:migrate -P 2
java -cp ~/.m2/repository/com/h2database/h2/1.4.196/h2-1.4.196.jar org.h2.tools.RunScript -url jdbc:h2:~/.filestore/filestore-2 -user sa -password sa -script ./src/main/resources/scripts/users.sql -showResults
java -jar ./target/jayblanc-1.0.0-SNAPSHOT-thorntail.jar -Dswarm.port.offset=200 -Dfilestore.instance.id=2 -Dfilestore.instance.name=Karmic_Wonderland  -Dswarm.datasources.data-sources.fsDS.connection-url=jdbc:h2:~/.filestore/filestore-2;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE
```

Open browser to : http://localhost:8280/fs/api/files  
or : http://localhost:8180/api/files/42/content

### Remove all db and files (fresh install) :

```bash
rm -Rf ~/.filestore/*
```

### Start dockerized application

Set the persistence layer to auto create database avoiding calling liquibase inside the container.

It is configured in the file: resources/META-INF/persistence.xml 

```xml
<!--<property name="javax.persistence.schema-generation.database.action" value="validate"/>-->
<property name="javax.persistence.schema-generation.database.action" value="create"/>            
```

Build the docker image using maven plugin.

```bash
mvn clean package dockerfile:build -DskipTests
```

Create local folders to host database and all persistent container data, allowing restart without data loss

```bash
sudo mkdir -P /var/filestores/sheldon
sudo chmod -R 777 /var/filestores
```

Run a specific instance of the filestore

```bash
sudo docker run -d -t -i -e FS_AUTH_URI='http://localhost:8080' \
-e FS_OWNER='sheldon' \ 
-e FS_ID='sheldon-fs' \
-e FS_NAME='Sheldon Cooper' \
-e FS_FQDN='http://sheldon.localhost' \
-v /var/filestores/sheldon:/opt/jboss/filestore \
--name sheldon-fs
dokku/filestore-1.0.0-SNAPSHOT
```

enjoy :-)


