package fr.miage.filestore.mediatheque.core;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.concurrent.ManagedThreadFactory;

import fr.miage.filestore.file.FileService;
import fr.miage.filestore.file.entity.FileItem;
import fr.miage.filestore.mediatheque.audio.AudioService;
import fr.miage.filestore.mediatheque.image.ImageService;
import fr.miage.filestore.mediatheque.video.VideoService;
import fr.miage.filestore.store.binary.BinaryStoreService;

@Startup
@Singleton
public class MediaServiceWorkerBean implements MediaServiceWorker {

	private static final Logger LOGGER = Logger.getLogger(MediaServiceWorkerBean.class.getName());

	@Resource
	private ManagedThreadFactory managedThreadFactory;

	@EJB
	FileService fileService;

	@EJB
	ImageService imageService;

    @EJB
    AudioService audioService;
   
   @EJB
   VideoService videoService;

	@EJB
	BinaryStoreService binaryStore;

	private JobWorkerThread worker;
	private Thread workerThread;
	private BlockingQueue<MediaJob> queue;

	@Override
	@PostConstruct
	public void start() {
		if (workerThread != null && workerThread.isAlive()) {
			LOGGER.log(Level.WARNING, "Media Job Worker already started");
			return;
		}
		LOGGER.log(Level.INFO, "Starting media job worker thread");
		worker = new JobWorkerThread();
		queue = new LinkedBlockingQueue<>();
		workerThread = managedThreadFactory.newThread(worker);
		workerThread.setName("Media Job Worker Thread");
		workerThread.start();
		Thread.UncaughtExceptionHandler h = (th, ex) -> {
			LOGGER.log(Level.SEVERE, "Uncaught exception", ex);
			start();
		};
		workerThread.setUncaughtExceptionHandler(h);
		LOGGER.log(Level.INFO, "Media Job worker thread started: " + workerThread.getState().toString());
	}

	@Override
	@PreDestroy
	public void stop() {
		LOGGER.log(Level.INFO, "Stopping media job worker thread");
		worker.stop();
	}

	@Override
	public List<MediaJob> getQueue() {
		return new ArrayList<>(queue);
	}

	@Override
	public void submit(String type, String item) {
		LOGGER.log(Level.INFO, "Submitting media job to worker");
		MediaJob job = new MediaJob();
		job.setId(UUID.randomUUID().toString());
		job.setStatus(MediaJob.Status.PENDING);
		job.setType(type);
		job.setItem(item);
		try {
			queue.put(job);
		} catch (InterruptedException e) {
			LOGGER.log(Level.INFO, "Unable to put media job in worker queue");
		}
	}

	class JobWorkerThread implements Runnable {

		private boolean run = true;

		void stop() {
			this.run = false;
		}

		@Override
		public void run() {
			while (run) {
				try {
					LOGGER.log(Level.INFO, "Media worker : Ready to take next job");
					MediaJob job = queue.take();
					LOGGER.log(Level.INFO, "Media worker : Take job: " + job.getId());
					try {
						StringBuffer report = new StringBuffer();

						if(job.getType().equals("file.create") || job.getType().equals("file.rescan")) {						
							FileItem item = fileService.get(job.getItem());
							
							if(item.getMimeType().startsWith("image")) {
								LOGGER.log(Level.INFO, "Media worker : New file is image");								
								imageService.add(item.getId(), item.getName(), item.getMimeType(), item.getSize(), item.getContentId());								
							} else if(item.getMimeType().startsWith("audio")) {
								LOGGER.log(Level.INFO, "Media worker : New file is audio");
								audioService.add(item.getId(), item.getName(), item.getMimeType(), item.getContentId());
							} else if(item.getMimeType().startsWith("video")) {
								LOGGER.log(Level.INFO, "Media worker : New file is video");
								videoService.add(item.getId(), item.getName(), item.getMimeType(), item.getContentId(), item.getSize());
							} else {
								LOGGER.log(Level.INFO, "Media worker : New file is not a media");
							}
						} else if(job.getType().equals("file.remove")) {
							if(imageService.exists(job.getItem())) {
								LOGGER.log(Level.INFO, "Media worker : Removed file is image");				
								imageService.remove(job.getItem());								
							} else if(audioService.exists(job.getItem())) {
								LOGGER.log(Level.INFO, "Media worker : Removed file is audio");
								audioService.remove(job.getItem());	
							} else if(videoService.exists(job.getItem())) {
								LOGGER.log(Level.INFO, "Media worker : Removed file is video");
								videoService.remove(job.getItem());	
							} else {
								LOGGER.log(Level.INFO, "Media worker : Removed file is not a media");
							}
						}

						report.append("Media worker : Job done.");
						job.setOutput(report.toString());
					} catch (Exception e) {
						LOGGER.log(Level.WARNING, "Media worker : Something wrong happened: " + e.getMessage(), e);
					}
					LOGGER.log(Level.INFO, "Job done.");
				} catch (InterruptedException e) {
					if (run) {
						LOGGER.log(Level.SEVERE, "Media worker : Interrupted while trying to take next job", e);
					} else {
						LOGGER.log(Level.INFO, "Media worker : Worker interrupted");
					}
				}
			}
			LOGGER.log(Level.INFO, "Media worker : End of life");
		}
	}
}
