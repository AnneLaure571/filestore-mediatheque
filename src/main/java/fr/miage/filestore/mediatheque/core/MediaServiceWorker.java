package fr.miage.filestore.mediatheque.core;

import java.util.List;

import fr.miage.filestore.store.index.IndexStoreJob;

public interface MediaServiceWorker {

	void start();

	void stop();

	List<MediaJob> getQueue();
	
	void submit(String eventType, String sourceId);

}
