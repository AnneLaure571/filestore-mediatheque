package fr.miage.filestore.mediatheque.core;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;
import org.jcodec.api.FrameGrab;
import org.jcodec.api.JCodecException;
import org.jcodec.common.Demuxer;
import org.jcodec.common.DemuxerTrack;
import org.jcodec.common.DemuxerTrackMeta;
import org.jcodec.common.Format;
import org.jcodec.common.JCodecUtil;
import org.jcodec.common.model.ColorSpace;
import org.jcodec.common.model.Picture;
import org.jcodec.scale.ColorUtil;
import org.jcodec.scale.RgbToBgr;
import org.jcodec.scale.Transform;

import net.bull.javamelody.internal.common.LOG;

public class MediaTools {
	
	private static final Logger LOGGER = Logger.getLogger(MediaTools.class.getName());
	
	public static ArrayList<String> videoToThumbnails(String inputPath) {
		LOG.info("*********************************************************");
		LOG.info("inputPath  :  " + inputPath);
		ArrayList<String> images = new ArrayList<>();
        File file = Paths.get(inputPath).toFile();
        try {
        	Format f = JCodecUtil.detectFormat(file);
        	Demuxer d = JCodecUtil.createDemuxer(f, file);
        	DemuxerTrack vt = d.getVideoTracks().get(0);
        	DemuxerTrackMeta dtm = vt.getMeta();
        	int nFrames = dtm.getTotalFrames();
        	
        	File imageFrame = createThumbnailFromVideo(file, 5);
        	InputStream targetStream = new FileInputStream(imageFrame);
            images.add(encodeToBase64(targetStream));
            
        	for (int i = 1; i < 6; i++) {
        		try {
                    imageFrame = createThumbnailFromVideo(file, i*nFrames/10);
                    targetStream = new FileInputStream(imageFrame);
                    images.add(encodeToBase64(targetStream));
        		} catch (Exception e) {
        			
        		}
			}
        } catch (IOException | JCodecException e) {
            System.out.println("error occurred while extracting image : " + e.getMessage());
        }
        LOG.info("images.size()  :  " + images.size());
		LOG.info("*********************************************************");
		return images;
	}
	
	public static File createThumbnailFromVideo(File file, int frameNumber) throws IOException, JCodecException {
		Picture frame = FrameGrab.getFrameFromFile(file, frameNumber);
        File tempFile = File.createTempFile("thumb_" + frameNumber + file.getName().replaceAll("(.+)\\..+", "$1"), ".png");
        ImageIO.write(toBufferedImage8Bit(frame), "png", tempFile);
        return tempFile;
    }
	
    // this method is from Jcodec AWTUtils.java.
    private static BufferedImage toBufferedImage8Bit(Picture src) {
        if (src.getColor() != ColorSpace.RGB) {
            Transform transform = ColorUtil.getTransform(src.getColor(), ColorSpace.RGB);
            if (transform == null) {
                throw new IllegalArgumentException("Unsupported input colorspace: " + src.getColor());
            }
            Picture out = Picture.create(src.getWidth(), src.getHeight(), ColorSpace.RGB);
            transform.transform(src, out);
            new RgbToBgr().transform(out, out);
            src = out;
        }
        BufferedImage dst = new BufferedImage(src.getCroppedWidth(), src.getCroppedHeight(),
                BufferedImage.TYPE_3BYTE_BGR);
        if (src.getCrop() == null)
            toBufferedImage8Bit2(src, dst);
        else
            toBufferedImageCropped8Bit(src, dst);
        return dst;
    }
    
    // this method is from Jcodec AWTUtils.java.
    private static void toBufferedImage8Bit2(Picture src, BufferedImage dst) {
        byte[] data = ((DataBufferByte) dst.getRaster().getDataBuffer()).getData();
        byte[] srcData = src.getPlaneData(0);
        for (int i = 0; i < data.length; i++) {
            data[i] = (byte) (srcData[i] + 128);
        }
    }
    
    // this method is from Jcodec AWTUtils.java.
    private static void toBufferedImageCropped8Bit(Picture src, BufferedImage dst) {
        byte[] data = ((DataBufferByte) dst.getRaster().getDataBuffer()).getData();
        byte[] srcData = src.getPlaneData(0);
        int dstStride = dst.getWidth() * 3;
        int srcStride = src.getWidth() * 3;
        for (int line = 0, srcOff = 0, dstOff = 0; line < dst.getHeight(); line++) {
            for (int id = dstOff, is = srcOff; id < dstOff + dstStride; id += 3, is += 3) {
                data[id] = (byte) (srcData[is] + 128);
                data[id + 1] = (byte) (srcData[is + 1] + 128);
                data[id + 2] = (byte) (srcData[is + 2] + 128);
            }
            srcOff += srcStride;
            dstOff += dstStride;
        }
    }

	public static InputStream resizeImage(BufferedImage sourceImage, int width, int height, String formatName) throws IOException {
	    
	    if(width > height) height = sourceImage.getHeight() * width / sourceImage.getWidth();
	    	else width = sourceImage.getWidth()* height / sourceImage.getHeight();
	    
	    Image thumbnail = sourceImage.getScaledInstance(width, height, Image.SCALE_SMOOTH);
	    BufferedImage bufferedThumbnail = new BufferedImage(thumbnail.getWidth(null),
	            thumbnail.getHeight(null),
	            BufferedImage.TYPE_INT_RGB);
	    bufferedThumbnail.getGraphics().drawImage(thumbnail, 0, 0, null);
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    ImageIO.write(bufferedThumbnail, formatName, baos);
	    return new ByteArrayInputStream(baos.toByteArray());
	}
	
	public static String encodeToBase64(InputStream stream) throws IOException {
		LOGGER.log(Level.INFO, "Encode image to base64");
		return Base64.getEncoder().encodeToString(IOUtils.toByteArray(stream));
	}
}
