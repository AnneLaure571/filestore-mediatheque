package fr.miage.filestore.mediatheque.core;

public class MediaItemNotFoundException extends Exception {

    public MediaItemNotFoundException(String message) {
        super(message);
    }
}
