package fr.miage.filestore.mediatheque.core;

import fr.miage.filestore.notification.entity.Event;
import fr.miage.filestore.store.index.IndexStoreListenerBean;
import fr.miage.filestore.store.index.IndexStoreServiceWorker;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import java.util.logging.Level;
import java.util.logging.Logger;

@MessageDriven(name = "NewFileListener", activationConfig = {
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "jms/topic/notification"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic"),
})
public class MediaListener implements MessageListener {

    private static final Logger LOGGER = Logger.getLogger(MediaListener.class.getName());
    
    @EJB
    private MediaServiceWorker worker;

    @Override
    public void onMessage(Message message) {
        try {
            LOGGER.log(Level.INFO, "Worker media listener message received");
            
            // We don't need to do anything if message concerns a folder
            if(message.getStringProperty(Event.EVENT_TYPE).startsWith("file")) { 
	            Event event = Event.fromJMSMessage(message);
	            worker.submit(event.getEventType(), event.getSourceId());
            }
            
        } catch (JMSException e) {
            LOGGER.log(Level.SEVERE, "error while triggering media worker", e);
        }
    }
}