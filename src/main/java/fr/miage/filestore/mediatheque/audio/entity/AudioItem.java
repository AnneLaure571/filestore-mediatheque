package fr.miage.filestore.mediatheque.audio.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Lob;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@NamedQueries({ 
	@NamedQuery(name = "AudioItem.listAll", query = "SELECT i FROM AudioItem i")
})
public class AudioItem implements Serializable {

	@Id
	@JsonProperty
	private String id;
	private String filename;
    private String title;
    private String author;
    private String artist;
    private String album;
	private Double duration;
	private String mimeType;
	private String genre;
	private int releaseDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	@JsonIgnore
	private String contentId;
	@Lob
	private HashMap<String, String> musicData;
	@Lob
	private String thumbnail;

	public AudioItem() {
		super();
	}
	
	public AudioItem(String fileID, String filename, String type, String contentId) {
		this.id = fileID;
		this.filename = filename;
		this.mimeType = type;
		this.contentId = contentId;
		this.musicData = new HashMap<>();
	}

//	public AudioItem(String id, String title, String author, String artist, String album, long duration,
//			String mimeType, Date creationDate, String contentId, String thumbnail) {
//		super();
//		this.id = id;
//		this.title = title;
//		this.author = author;
//		this.artist = artist;
//		this.album = album;
//		this.duration = duration;
//		this.mimeType = mimeType;
//		this.creationDate = creationDate;
//		this.contentId = contentId;
//		this.thumbnail = thumbnail;
//	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getfileName() {
		return filename;
	}

	public void setfileName(String filename) {
		this.filename = filename;
	}


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getArtist() {
		return artist;
	}

	public void setArtist(String artist) {
		this.artist = artist;
	}

	public String getAlbum() {
		return album;
	}

	public void setAlbum(String album) {
		this.album = album;
	}

	public int getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(int releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Double getDuration() {
		return duration;
	}

	public void setDuration(Double duration) {
		this.duration = duration;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public HashMap<String, String> getMusicData() {
		return musicData;
	}

	public void setMusicData(HashMap<String, String> musicData) {
		this.musicData = musicData;
	}

	public void addMusicData(String key, String value) {
		this.musicData.put(key, value);
	}

	@Override
	public String toString() {
		return "AudioItem [id=" + id + ", title=" + title + ", author=" + author + ", artist=" + artist + ", album="
				+ album + ", duration=" + duration + ", mimeType=" + mimeType + ", creationDate=" + creationDate
				+ ", contentId=" + contentId + ", thumbnail=" + thumbnail + "]";
	}

	
}
