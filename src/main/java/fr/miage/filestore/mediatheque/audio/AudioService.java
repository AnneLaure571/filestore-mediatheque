package fr.miage.filestore.mediatheque.audio;

import java.util.List;

import fr.miage.filestore.mediatheque.audio.entity.AudioItem;
import fr.miage.filestore.mediatheque.core.MediaItemNotFoundException;
import fr.miage.filestore.store.binary.BinaryStoreServiceException;
import fr.miage.filestore.store.binary.BinaryStreamNotFoundException;

public interface AudioService {
	
	//List all audioItem
	List<AudioItem> listAll() throws MediaItemNotFoundException;

    //Add new audioItem
    AudioItem add(String id, String filename, String type, String contentId)
			throws BinaryStoreServiceException, BinaryStreamNotFoundException;
    
    AudioItem get(String id) throws MediaItemNotFoundException;

    void remove(String id) throws MediaItemNotFoundException;

	boolean exists(String item);

	String getThumbnail(String id);

	void deleteAll();

}
