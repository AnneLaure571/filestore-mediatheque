package fr.miage.filestore.mediatheque.audio;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.TikaCoreProperties;

import fr.miage.filestore.mediatheque.audio.entity.AudioItem;
import fr.miage.filestore.mediatheque.core.MediaItemNotFoundException;
import fr.miage.filestore.mediatheque.core.MediaTools;
import fr.miage.filestore.store.binary.BinaryStoreService;
import fr.miage.filestore.store.binary.BinaryStoreServiceException;
import fr.miage.filestore.store.binary.BinaryStreamNotFoundException;

@Stateless
public class AudioServiceBean implements AudioService {

	private static final Logger LOGGER = Logger.getLogger(AudioService.class.getName());

	@EJB
	private BinaryStoreService binaryStore;

	@PersistenceContext(unitName = "fsPU")
	private EntityManager em;

	// method to list all audios files
	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<AudioItem> listAll() throws MediaItemNotFoundException {
		LOGGER.log(Level.INFO, "List all audios: ");
		TypedQuery<AudioItem> query = em.createNamedQuery("AudioItem.listAll", AudioItem.class);
		List<AudioItem> audios = query.getResultList();
		LOGGER.log(Level.INFO, "query returned " + audios.size() + " results");
		return audios;
	}

	// method to add new audio item
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public AudioItem add(String id, String filename, String type, String contentId)
			throws BinaryStoreServiceException, BinaryStreamNotFoundException {
		AudioItem audioFile = new AudioItem(id, filename, type, contentId);

		Metadata metadata = binaryStore.parseMetadata(contentId);

		audioFile.setCreationDate(metadata.getDate(TikaCoreProperties.CREATED));

		// if is useful ?
		checkMetadata(metadata, audioFile);

		// Get list
		for (String dataname : metadata.names()) {
			if (dataname.startsWith("xmpDM:")) {
				audioFile.addMusicData(dataname.substring(6).strip(), metadata.get(dataname));
			}
		}
		
		em.persist(audioFile);
		LOGGER.log(Level.FINE, "Adding audio " + audioFile);
		return audioFile;
	}

	// get audio item
	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public AudioItem get(String id) throws MediaItemNotFoundException {
		LOGGER.log(Level.INFO, "Getting audio with id: " + id);
		AudioItem item = em.find(AudioItem.class, id);
		if (item == null) {
			throw new MediaItemNotFoundException("unable to find a audio with id: " + id);
		}
		return item;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remove(String id) throws MediaItemNotFoundException {
		/*
		 * LOGGER.log(Level.FINE, "Removing audio with id " + id); AudioItem item =
		 * em.find(AudioItem.class, id); if (item == null) { throw new
		 * MediaItemNotFoundException("Audio to remove was not found"); } try { //TODO
		 * Check here binaryStore.delete(item.getThumbnail()); } catch
		 * (BinaryStoreServiceException | BinaryStreamNotFoundException e) {
		 * LOGGER.log(Level.WARNING, "Error while removing audio thumbnail"); }
		 * em.remove(item);
		 */
		LOGGER.log(Level.FINE, "Removing audio with id " + id);
		AudioItem item = em.find(AudioItem.class, id);
		if (item == null) {
			throw new MediaItemNotFoundException("Audio to remove was not found");
		}
		em.remove(item);
	}

	// check if audio file/ audio item exists
	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public boolean exists(String id) {
		AudioItem item = em.find(AudioItem.class, id);
		return item != null;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public String getThumbnail(String id) {
		LOGGER.log(Level.INFO, "Getting thumbnail for image with id: " + id);
		AudioItem item = em.find(AudioItem.class, id);
		InputStream thumbnail;
		try {
			thumbnail = binaryStore.get(item.getThumbnail());
			return MediaTools.encodeToBase64(thumbnail);
		} catch (BinaryStoreServiceException | BinaryStreamNotFoundException | IOException e) {
			LOGGER.log(Level.WARNING, "Error while getting image thumbnail");
			return null;
		}
	}

	public void checkMetadata(Metadata metadata, AudioItem audioFile) {
		if (metadata.get("title") != null) {
			audioFile.setTitle(metadata.get("title"));
		} else {
			audioFile.setTitle("N/A");
		}
		if (metadata.get("meta:author") != null) {
			audioFile.setAuthor(metadata.get("meta:author"));
		} else {
			audioFile.setAuthor("N/A");
		}
		if (metadata.get("xmpDM:album") != null) {
			audioFile.setAlbum(metadata.get("xmpDM:album"));
		} else {
			audioFile.setAlbum("N/A");
		}
		if (metadata.get("xmpDM:artist") != null) {
			audioFile.setArtist(metadata.get("xmpDM:artist"));
		} else {
			audioFile.setArtist("N/A");
		}
		if (metadata.get("xmpDM:releaseDate") != null) {
			audioFile.setReleaseDate(Integer.parseInt(metadata.get("xmpDM:releaseDate")));
		} else {
			audioFile.setReleaseDate(0);
		}
		if (metadata.get("xmpDM:genre") != null) {
			audioFile.setGenre(metadata.get("xmpDM:genre"));
		} else {
			audioFile.setGenre("N/A");
		}
		if (metadata.get("xmpDM:duration") != null) {
			String duration = metadata.get("xmpDM:duration");
			if (duration.contains(",")) {
				duration = duration.replace(",", ".");
				LOGGER.log(Level.INFO, "Durée " + duration);
				audioFile.setDuration(Double.valueOf(duration));
			} else {
				audioFile.setDuration(Double.valueOf(metadata.get("xmpDM:duration")));
			}
		} else {
			audioFile.setDuration(0.0);
		}
	}

	@Override
	public void deleteAll() {
		em.createQuery("DELETE FROM AudioItem").executeUpdate();
	}
}
