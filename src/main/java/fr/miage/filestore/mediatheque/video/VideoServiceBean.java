package fr.miage.filestore.mediatheque.video;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.TikaCoreProperties;

import fr.miage.filestore.mediatheque.core.MediaItemNotFoundException;
import fr.miage.filestore.mediatheque.core.MediaTools;
import fr.miage.filestore.mediatheque.video.entity.VideoItem;
import fr.miage.filestore.store.binary.BinaryStoreService;
import fr.miage.filestore.store.binary.BinaryStoreServiceException;
import fr.miage.filestore.store.binary.BinaryStreamNotFoundException;

@Stateless
public class VideoServiceBean implements VideoService {

	private static final Logger LOGGER = Logger.getLogger(VideoService.class.getName());

	@EJB
	private BinaryStoreService binaryStore;

	@PersistenceContext(unitName = "fsPU")
	private EntityManager em;

	// lister toutes les videos
	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<VideoItem> listAll() throws MediaItemNotFoundException {
		LOGGER.log(Level.INFO, "List all videos: ");
		TypedQuery<VideoItem> query = em.createNamedQuery("VideoItem.listAll", VideoItem.class);
		List<VideoItem> videos = query.getResultList();
		LOGGER.log(Level.INFO, "query returned " + videos.size() + " results");
		return videos;
	}
	
	//ajouter une video
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public VideoItem add(String id, String filename, String type, String contentId, long size)
			throws BinaryStoreServiceException, BinaryStreamNotFoundException {
        VideoItem videoFile = new VideoItem(id, filename, type, contentId, size);

		Metadata metadata = binaryStore.parseMetadata(contentId);
		
		videoFile.setCreationDate(metadata.getDate(TikaCoreProperties.CREATED));
		if(metadata.get("title") != null) videoFile.setTitle(metadata.get("title"));
		if(metadata.get("xmpDM:duration") != null) videoFile.setDuration(Double.parseDouble(metadata.get("xmpDM:duration")));
		if(metadata.get("tiff:ImageLength") != null) videoFile.setHeight(Integer.parseInt(metadata.get("tiff:ImageLength")));
		if(metadata.get("tiff:ImageWidth") != null)  videoFile.setWidth(Integer.parseInt(metadata.get("tiff:ImageWidth")));

		try {
			ArrayList<String> thumbnails = MediaTools.videoToThumbnails(binaryStore.filePath(contentId));
			videoFile.setThumbnails(thumbnails);
		} catch (BinaryStoreServiceException | BinaryStreamNotFoundException e) {
			
		}
		
//		for (String dataname : metadata.names()) {
//			if (dataname.startsWith("tiff:")) {
//				videoFile.addTiffdata(dataname.substring(5).strip(), metadata.get(dataname));
//			} else if (dataname.startsWith("exif:")) {
//				videoFile.addExifdata(dataname.substring(5).strip(), metadata.get(dataname));
//			}
//		}
		
		em.persist(videoFile);
		LOGGER.log(Level.FINE, "Adding video " + videoFile);
		return videoFile;
	}

	//get video item
	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public VideoItem get(String id) throws MediaItemNotFoundException {
		LOGGER.log(Level.INFO, "Getting video with id: " + id);
		VideoItem item = em.find(VideoItem.class, id);
		if (item == null) {
			throw new MediaItemNotFoundException("unable to find a video with id: " + id);
		}
		return item;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remove(String id) throws MediaItemNotFoundException {
		LOGGER.log(Level.FINE, "Removing video with id " + id);
		VideoItem item = em.find(VideoItem.class, id);
		if (item == null) {
			throw new MediaItemNotFoundException("Video to remove was not found");
		}
		em.remove(item);
	}

	//check if video file/ video item exists
	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public boolean exists(String id) {
		VideoItem item = em.find(VideoItem.class, id);
		return item != null;
	}

	@Override
	public void deleteAll() {
		em.createQuery("DELETE FROM VideoItem").executeUpdate();		
	}
}
