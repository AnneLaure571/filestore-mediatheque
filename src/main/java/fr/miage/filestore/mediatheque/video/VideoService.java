package fr.miage.filestore.mediatheque.video;

import java.util.List;

import fr.miage.filestore.mediatheque.core.MediaItemNotFoundException;
import fr.miage.filestore.mediatheque.video.entity.VideoItem;
import fr.miage.filestore.store.binary.BinaryStoreServiceException;
import fr.miage.filestore.store.binary.BinaryStreamNotFoundException;

public interface VideoService {
	
	List<VideoItem> listAll() throws MediaItemNotFoundException;

    VideoItem add(String id, String filename, String type, String contentId, long size)
			throws BinaryStoreServiceException, BinaryStreamNotFoundException;
    
    VideoItem get(String id) throws MediaItemNotFoundException;

    void remove(String id) throws MediaItemNotFoundException;

	boolean exists(String item);

	void deleteAll();
}
