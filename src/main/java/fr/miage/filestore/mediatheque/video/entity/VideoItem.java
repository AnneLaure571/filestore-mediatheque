package fr.miage.filestore.mediatheque.video.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Lob;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@NamedQueries({ 
	@NamedQuery(name = "VideoItem.listAll", query = "SELECT i FROM VideoItem i")
})
public class VideoItem implements Serializable {

	@Id
	@JsonProperty
	private String id;
	private String filename;
    private String title;
	private long size;
	private Double duration;
	private String mimeType;
	private int releaseDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	@JsonIgnore
	private String contentId;
	@Lob
	private ArrayList<String> thumbnails;
	
	private Integer width;
	private Integer height;

	public VideoItem() {
		super();
	}
	
	public VideoItem(String fileID, String filename, String type, String contentId, long size) {
		this.id = fileID;
		this.filename = filename;
		this.mimeType = type;
		this.contentId = contentId;
		this.size = size;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getfileName() {
		return filename;
	}

	public void setfileName(String filename) {
		this.filename = filename;
	}


	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}

	public int getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(int releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Double getDuration() {
		return duration;
	}

	public void setDuration(Double duration) {
		this.duration = duration;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public ArrayList<String> getThumbnails() {
		return thumbnails;
	}

	public void setThumbnails(ArrayList<String> thumbnails) {
		this.thumbnails = thumbnails;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	@Override
	public String toString() {
        return "AudioItem [id=" + id + ", title=" + title + ", size=" + size +
                ", duration=" + duration + ", mimeType=" + mimeType + ", creationDate=" + creationDate
				+ ", contentId=" + contentId + "]";
	}

	
}
