package fr.miage.filestore.mediatheque.image;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.tika.metadata.Metadata;
import org.apache.tika.metadata.TikaCoreProperties;

import fr.miage.filestore.mediatheque.core.MediaItemNotFoundException;
import fr.miage.filestore.mediatheque.core.MediaTools;
import fr.miage.filestore.mediatheque.image.entity.ImageItem;
import fr.miage.filestore.mediatheque.image.entity.TagItem;
import fr.miage.filestore.store.binary.BinaryStoreService;
import fr.miage.filestore.store.binary.BinaryStoreServiceException;
import fr.miage.filestore.store.binary.BinaryStreamNotFoundException;

@Stateless
public class ImageServiceBean implements ImageService {

	private static final Logger LOGGER = Logger.getLogger(ImageService.class.getName());

	@EJB
	private BinaryStoreService binaryStore;

	@PersistenceContext(unitName = "fsPU")
	private EntityManager em;

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<ImageItem> listAll() throws MediaItemNotFoundException {
		LOGGER.log(Level.INFO, "List all images: ");
		TypedQuery<ImageItem> query = em.createNamedQuery("ImageItem.listAll", ImageItem.class);
		List<ImageItem> images = query.getResultList();
		LOGGER.log(Level.INFO, "query returned " + images.size() + " results");
		return images;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public ImageItem add(String id, String name, String type, long size, String contentId)
			throws BinaryStoreServiceException, BinaryStreamNotFoundException {
		ImageItem image = new ImageItem(id, name, type, size, contentId);

		Metadata metadata = binaryStore.parseMetadata(contentId);

		image.setCreationDate(metadata.getDate(TikaCoreProperties.CREATED));

		if (type.equals("image/webp")) {
			image.setHeight(Integer.parseInt(metadata.get("Image Height")));
			image.setWidth(Integer.parseInt(metadata.get("Image Width")));
		} else {
			image.setHeight(Integer.parseInt(metadata.get("tiff:ImageLength")));
			image.setWidth(Integer.parseInt(metadata.get("tiff:ImageWidth")));
		}

		for (String dataname : metadata.names()) {
			if (dataname.startsWith("tiff:")) {
				image.addTiffdata(dataname.substring(5).strip(), metadata.get(dataname));
			} else if (dataname.startsWith("exif:")) {
				image.addExifdata(dataname.substring(5).strip(), metadata.get(dataname));
			}
		}

		try {
			InputStream thumbnail = MediaTools.resizeImage(ImageIO.read(binaryStore.get(contentId)), 250, 200,
					type.substring(image.getMimeType().lastIndexOf("/") + 1));
			String base64Image = MediaTools.encodeToBase64(thumbnail);
			image.setThumbnail(base64Image);
		} catch (IOException | BinaryStoreServiceException | BinaryStreamNotFoundException e) {
			e.printStackTrace();
		}

		em.persist(image);

		return image;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public ImageItem get(String id) throws MediaItemNotFoundException {
		LOGGER.log(Level.INFO, "Getting image with id: " + id);
		ImageItem item = em.find(ImageItem.class, id);
		if (item == null) {
			throw new MediaItemNotFoundException("unable to find a iamge with id: " + id);
		}
		return item;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remove(String id) throws MediaItemNotFoundException {
		LOGGER.log(Level.INFO, "Removing image with id " + id);
		ImageItem item = em.find(ImageItem.class, id);
		if (item == null) {
			throw new MediaItemNotFoundException("Image to remove was not found");
		}
		em.remove(item);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public boolean exists(String id) {
		ImageItem item = em.find(ImageItem.class, id);
		return item != null;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean addTag(String id, String tagname) {
		LOGGER.log(Level.INFO, "Add tag on image : " + tagname);
		ImageItem image = em.find(ImageItem.class, id);
		if (image != null) {
			TypedQuery<TagItem> query = em.createNamedQuery("TagItem.findByName", TagItem.class).setParameter("name",
					tagname);
			List<TagItem> tags = query.getResultList();
			TagItem tag;
			if (tags.isEmpty()) {
				tag = new TagItem(UUID.randomUUID().toString(), tagname);
				em.persist(tag);
			} else
				tag = tags.get(0);
			image.addTag(tag);
			em.persist(image);
		} else {
			return false;
		}
		return true;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public boolean removeTag(String id, String tagname) {
		LOGGER.log(Level.INFO, "Remove tag on image : " + tagname);
		ImageItem image = em.find(ImageItem.class, id);

		if (image != null) {
			TypedQuery<TagItem> query = em.createNamedQuery("TagItem.findByName", TagItem.class).setParameter("name",
					tagname);
			List<TagItem> tags = query.getResultList();
			LOGGER.log(Level.INFO, "Found results for tag name : " + tags.size());
			TagItem tag;
			if ((tag = tags.get(0)) != null) {
				image.removeTag(tag);
				em.persist(image);

				TypedQuery<ImageItem> references = em.createNamedQuery("ImageItem.findByTagName", ImageItem.class)
						.setParameter("name", tagname);
				List<ImageItem> occurences = references.getResultList();
				LOGGER.log(Level.INFO, "Found references for tag name : " + occurences.size());
				if (occurences.isEmpty()) {
					em.remove(tag);
				}
			} else {
				LOGGER.log(Level.INFO, "tag not found");
			}
		} else {
			return false;
		}
		return true;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public List<TagItem> getAllTags() {
		TypedQuery<TagItem> query = em.createNamedQuery("TagItem.listAll", TagItem.class);
		List<TagItem> tags = query.getResultList();
		LOGGER.log(Level.INFO, "Tags found : " + tags.size());
		return tags;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void deleteAll() {
		em.createQuery("DELETE FROM ImageItem").executeUpdate();
	}
}
