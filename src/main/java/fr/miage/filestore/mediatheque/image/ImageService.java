package fr.miage.filestore.mediatheque.image;

import java.util.List;

import fr.miage.filestore.mediatheque.core.MediaItemNotFoundException;
import fr.miage.filestore.mediatheque.image.entity.ImageItem;
import fr.miage.filestore.mediatheque.image.entity.TagItem;
import fr.miage.filestore.store.binary.BinaryStoreServiceException;
import fr.miage.filestore.store.binary.BinaryStreamNotFoundException;


public interface ImageService {
	
	List<ImageItem> listAll() throws MediaItemNotFoundException;

    ImageItem add(String id, String name, String type, long size, String contentId)
			throws BinaryStoreServiceException, BinaryStreamNotFoundException;
    
    ImageItem get(String id) throws MediaItemNotFoundException;
	
    void remove(String id) throws MediaItemNotFoundException;

	boolean exists(String item);
	
	List<TagItem> getAllTags();

	boolean addTag(String id, String tag);
	
	boolean removeTag(String id, String tag);

	void deleteAll();
}
