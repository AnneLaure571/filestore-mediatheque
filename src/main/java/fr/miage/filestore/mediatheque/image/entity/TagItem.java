package fr.miage.filestore.mediatheque.image.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@NamedQueries({ 
	@NamedQuery(name = "TagItem.listAll", query = "SELECT t FROM TagItem t "),
	@NamedQuery(name = "TagItem.findByName", query = "SELECT t FROM TagItem t WHERE t.name = :name ")
})
public class TagItem implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@JsonIgnore
	private String id;
	private String name;	
	
	@ManyToMany(mappedBy = "tags", cascade = {
            CascadeType.MERGE,
            CascadeType.REFRESH
        })
	private List<ImageItem> images = new ArrayList<>();
	
	public TagItem() {
		super();
	}

	public TagItem(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}	
}
