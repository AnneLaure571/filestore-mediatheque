package fr.miage.filestore.mediatheque.image.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@NamedQueries({ 
	@NamedQuery(name = "ImageItem.listAll", query = "SELECT i FROM ImageItem i "),
	@NamedQuery(name = "ImageItem.removeAll", query = "DELETE FROM ImageItem "),
	@NamedQuery(name = "ImageItem.findByTagName", query = "SELECT i FROM ImageItem i JOIN i.tags t WHERE t.name = :name ")
})
public class ImageItem implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@JsonProperty
	private String id;
	private String name;
	private String mimeType;
	private long size;
	private Integer width;
	private Integer height;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	
	@JsonIgnore
	private String contentId;
	
	@Lob
	private HashMap<String, String> exifdata;
	
	@Lob
	private HashMap<String, String> tiffdata;
	

	@ManyToMany(fetch = FetchType.EAGER, cascade = {
            CascadeType.MERGE,
            CascadeType.REFRESH
        })
    @JoinTable(
            name = "TAGS",
            joinColumns = {@JoinColumn(name = "id_image", referencedColumnName="id")},
            inverseJoinColumns = {@JoinColumn(name = "id_tag", referencedColumnName="id")}
    )
	private List<TagItem> tags;
	
	@Lob
	private String thumbnail;


	public ImageItem() {
		super();
	}

	public ImageItem(String fileID, String name, String type, long size, String contentId) {
		this.id = fileID;
		this.name = name;
		this.mimeType = type;
		this.size = size;
		this.contentId = contentId;
		this.tiffdata = new HashMap<>();
		this.exifdata = new HashMap<>();
		this.tags = new ArrayList<>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public Integer getWidth() {
		return width;
	}

	public void setWidth(Integer width) {
		this.width = width;
	}

	public Integer getHeight() {
		return height;
	}

	public void setHeight(Integer height) {
		this.height = height;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	
	public HashMap<String, String> getExifdata() {
		return exifdata;
	}

	public void setExifdata(HashMap<String, String> exifdata) {
		this.exifdata = exifdata;
	}
	
	public void addExifdata(String key, String value) {
		this.exifdata.put(key, value);
	}

	public HashMap<String, String> getTiffdata() {
		return tiffdata;
	}

	public void setTiffdata(HashMap<String, String> tiffdata) {
		this.tiffdata = tiffdata;
	}
	
	public void addTiffdata(String key, String value) {
		this.tiffdata.put(key, value);
	}
	
	public List<TagItem> getTags() {
		return tags;
	}

	public void setTags(List<TagItem> tags) {
		this.tags = tags;
	}
	
	public void addTag(TagItem tag) {
		this.tags.add(tag);
	}
	
	public void removeTag(TagItem tag) {
		this.tags.remove(tag);
	}

	@Override
	public String toString() {
		return "ImageItem [id=" + id + ", name=" + name + ", mimeType=" + mimeType
				+ ", creationDate=" + creationDate + ", contentId=" + contentId
				+ ", width=" + width + ", height=" + height  + ", tags=" + tags + "]";
	}
}
