package fr.miage.filestore.api.filter;

import org.wildfly.swarm.spi.runtime.annotations.ConfigurationValue;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Provider
public class CORSFilter implements ContainerResponseFilter {

    @Inject
    @ConfigurationValue("filestore.api.cors.origin")
    private String origin;

    @Inject
    @ConfigurationValue("filestore.api.cors.methods")
    private String methods;

    @Inject
    @ConfigurationValue("filestore.api.cors.max-age")
    private String maxAge;

    @Inject
    @ConfigurationValue("filestore.api.cors.headers")
    private String headers;

    @Inject
    @ConfigurationValue("filestore.api.cors.expose-headers")
    private String exposeHeaders;


    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        responseContext.getHeaders().add("Access-Control-Allow-Origin", origin);
        responseContext.getHeaders().add("Access-Control-Allow-Methods", methods);
        responseContext.getHeaders().add("Access-Control-Max-Age", maxAge);
        responseContext.getHeaders().add("Access-Control-Allow-Headers", headers);
        responseContext.getHeaders().add("Access-Control-Expose-Headers", exposeHeaders);
    }

}
