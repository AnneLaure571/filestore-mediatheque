package fr.miage.filestore.api.resources;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import fr.miage.filestore.api.filter.OnlyOwner;
import fr.miage.filestore.auth.AuthenticationService;
import fr.miage.filestore.file.FileService;
import fr.miage.filestore.mediatheque.core.MediaItemNotFoundException;
import fr.miage.filestore.mediatheque.video.VideoService;
import fr.miage.filestore.mediatheque.video.entity.VideoItem;
import fr.miage.filestore.notification.NotificationServiceException;

@Path("videos")
@OnlyOwner
public class VideosResource {

	private static final Logger LOGGER = Logger.getLogger(VideosResource.class.getName());

	@EJB
	private FileService fs;
	
	@EJB
	private VideoService service;

	@EJB
	private AuthenticationService auth;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response root(@Context UriInfo uriInfo) throws MediaItemNotFoundException {
		LOGGER.log(Level.INFO, "GET /api/videos");
		List<VideoItem> videos = service.listAll();
        return Response.ok(videos).build();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public VideoItem get(@PathParam("id") String id) throws MediaItemNotFoundException {
		LOGGER.log(Level.INFO, "GET /api/videos/" + id);
		VideoItem item = service.get(id);
		return item;
	}
	
	@GET
	@Path("refresh")
	@Produces(MediaType.APPLICATION_JSON)
	public Response refresh() {
		LOGGER.log(Level.INFO, "GET /api/video/refresh");
		service.deleteAll();
		LOGGER.log(Level.INFO, "All videos were deleted");
		try {
			fs.rescan("video");
		} catch (NotificationServiceException e) {
			return Response.noContent().build();
		}
		return Response.ok().build();
	}
}
