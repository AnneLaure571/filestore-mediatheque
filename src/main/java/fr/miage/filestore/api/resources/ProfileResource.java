package fr.miage.filestore.api.resources;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import fr.miage.filestore.api.dto.ProfileData;
import fr.miage.filestore.api.filter.OnlyOwner;
import fr.miage.filestore.auth.AuthenticationService;
import fr.miage.filestore.auth.entity.Profile;
import fr.miage.filestore.file.FileItemNotFoundException;
import fr.miage.filestore.file.FileServiceException;

@Path("profile")
@OnlyOwner
public class ProfileResource {

    private static final Logger LOGGER = Logger.getLogger(ProfileResource.class.getName());

    @EJB
    private AuthenticationService auth;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public ProfileData root(@Context UriInfo uriInfo) throws FileItemNotFoundException, FileServiceException {
        LOGGER.log(Level.INFO, "GET /api/profile");
        return profileData();
    }
    
    private ProfileData profileData() {
    	ProfileData data = new ProfileData();
    	Profile profile = auth.getConnectedProfile();
        
    	data.setFullname(profile.getFullname());;
    	data.setEmail(profile.getEmail());
    	data.setGravatarHash(profile.getGravatarHash());

        return data;
    }
}