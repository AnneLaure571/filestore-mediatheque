package fr.miage.filestore.api.resources;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import fr.miage.filestore.api.filter.OnlyOwner;
import fr.miage.filestore.auth.AuthenticationService;
import fr.miage.filestore.file.FileItemNotFoundException;
import fr.miage.filestore.file.FileService;
import fr.miage.filestore.file.FileServiceException;
import fr.miage.filestore.file.entity.FileItem;
import fr.miage.filestore.mediatheque.core.MediaItemNotFoundException;
import fr.miage.filestore.mediatheque.image.ImageService;
import fr.miage.filestore.mediatheque.image.entity.ImageItem;
import fr.miage.filestore.mediatheque.image.entity.TagItem;
import fr.miage.filestore.notification.NotificationServiceException;

@Path("images")
@OnlyOwner
public class ImagesResources {

	private static final Logger LOGGER = Logger.getLogger(ImagesResources.class.getName());

	@EJB
	private FileService fs;
	
	@EJB
	private ImageService service;

	@EJB
	private AuthenticationService auth;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response root(@Context UriInfo uriInfo) throws MediaItemNotFoundException {
		LOGGER.log(Level.INFO, "GET /api/images");
		List<ImageItem> images = service.listAll();
        return Response.ok(images).build();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public ImageItem get(@PathParam("id") String id) throws MediaItemNotFoundException {
		LOGGER.log(Level.INFO, "GET /api/images/" + id);
		ImageItem item = service.get(id);
		return item;
	}
	
	@GET
	@Path("/tags")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTags() {
		LOGGER.log(Level.INFO, "GET /api/images/tags");
		List<TagItem> tags = service.getAllTags();
        return Response.ok(tags).build();
	}

	@POST
	@Path("{id}/tag/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addtag(@PathParam("id") String id, @PathParam("name") String name) throws MediaItemNotFoundException {
		LOGGER.log(Level.INFO, "Adding tag " + name + " to image " + id);
		service.addTag(id, name);
		return Response.ok().build();       
	}
	
	@DELETE
	@Path("{id}/tag/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response removetag(@PathParam("id") String id, @PathParam("name") String name) throws MediaItemNotFoundException {
		LOGGER.log(Level.INFO, "Removing tag from image");
		service.removeTag(id, name);
		return Response.ok().build();
	}
	
	@GET
	@Path("refresh")
	@Produces(MediaType.APPLICATION_JSON)
	public Response refresh() {
		LOGGER.log(Level.INFO, "GET /api/images/refresh");
		service.deleteAll();
		LOGGER.log(Level.INFO, "All images were deleted");
		try {
			fs.rescan("image");
		} catch (NotificationServiceException e) {
			return Response.noContent().build();
		}
		return Response.ok().build();
	}
}
