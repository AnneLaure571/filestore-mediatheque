package fr.miage.filestore.api.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.io.IOUtils;

import fr.miage.filestore.api.filter.OnlyOwner;
import fr.miage.filestore.api.template.Template;
import fr.miage.filestore.auth.AuthenticationService;
import fr.miage.filestore.file.FileService;
import fr.miage.filestore.mediatheque.core.MediaItemNotFoundException;
import fr.miage.filestore.notification.NotificationServiceException;
import fr.miage.filestore.mediatheque.audio.AudioService;
import fr.miage.filestore.mediatheque.audio.entity.AudioItem;

@Path("musics")
@OnlyOwner
public class AudiosResource {

	private static final Logger LOGGER = Logger.getLogger(AudiosResource.class.getName());

	@EJB
	private FileService fs;
	
	@EJB
	private AudioService service;

	@EJB
	private AuthenticationService auth;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response root(@Context UriInfo uriInfo) throws MediaItemNotFoundException {
		LOGGER.log(Level.INFO, "GET /api/musics");
		List<AudioItem> audios = service.listAll();
        return Response.ok(audios).build();
	}


	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public AudioItem get(@PathParam("id") String id) throws MediaItemNotFoundException {
		LOGGER.log(Level.INFO, "GET /api/musics/" + id);
		AudioItem item = service.get(id);
		return item;
	}
	
//	@GET
//	@Path("{id}/thumbnail")
//	public Response getThumbnail(@PathParam("id") String id) throws MediaItemNotFoundException {
//		LOGGER.log(Level.INFO, "GET thumbnail from /api/images/" + id + "/thumbnail");
//		ImageItem image = service.get(id);
//		
//		try {
//			return Response.ok(IOUtils.toByteArray(service.getThumbnail(id)))
//					.header("Inline", "filename=\"" + image.getName() + "\"")
//			        .header("Content-Type", image.getMimeType())
//			        .build();
//		} catch (IOException e) {
//			LOGGER.log(Level.WARNING, "Couldn't retreive thumbnail from /api/images/" + id + "/thumbnail");
//			return Response.noContent().build();
//		}
//	}
	
	@GET
	@Path("refresh")
	@Produces(MediaType.APPLICATION_JSON)
	public Response refresh() {
		LOGGER.log(Level.INFO, "GET /api/audio/refresh");
		service.deleteAll();
		LOGGER.log(Level.INFO, "All audios were deleted");
		try {
			fs.rescan("audio");
		} catch (NotificationServiceException e) {
			return Response.noContent().build();
		}
		return Response.ok().build();
	}
}
